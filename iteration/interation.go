package iteration

func Repeat(char string, frequency int) string {
	var repeated string

	for i := 0; i < frequency; i++ {
		repeated += char
	}

	return repeated
}

func SumUpTo(numbersUpTo int) (sum int) {
	for i := 0; i <= numbersUpTo; i++ {
		sum += i
	}
	return
}

func SumArray(numbers []int) int {
	sum := 0

	for _, num := range numbers {
		sum += num
	}
	return sum
}

func SumAll(numbersToSum ...[]int) []int {
	var sums []int

	for _, numbers := range numbersToSum {
		sums = append(sums, SumArray(numbers))
	}

	return sums
}

func SumAllTails(numbersToSum ...[]int) []int {
	var sumTails []int

	for _, numbers := range numbersToSum {
		if len(numbers) == 0 {
			sumTails = append(sumTails, 0)
		} else {
			tail := numbers[1:]
			sumTails = append(sumTails, SumArray(tail))
		}
	}
	return sumTails
}
