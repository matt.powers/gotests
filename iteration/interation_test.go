package iteration

import (
	"reflect"
	"testing"
)

func TestRepeat(t *testing.T) {
	t.Run("should get 5 a's for input of a, 5", func(t *testing.T) {
		repeated := Repeat("a", 5)
		expected := "aaaaa"

		if repeated != expected {
			t.Errorf("wanted %s got %s", expected, repeated)
		}
	})
	t.Run("should get empty string for any char with 0", func(t *testing.T) {
		repeated := Repeat("a", 0)
		expected := ""

		if repeated != expected {
			t.Errorf("wanted %s got %s", expected, repeated)
		}
	})
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 10000)
	}
}

func TestSumArray(t *testing.T) {
	t.Run("SumUpTo will return 15 given 5", func(t *testing.T) {
		number := 5

		sum := SumUpTo(number)
		want := 15

		if sum != want {
			t.Errorf("got %d want %d, given %v", sum, want, number)
		}
	})

	t.Run("SumArray will return 15 given [1,2,3,4,5]", func(t *testing.T) {
		given := []int{1, 2, 3, 4, 5}

		want := 15

		got := SumArray(given)

		if got != want {
			t.Errorf("got %d want %d given %v", got, want, given)
		}
	})
}

func TestSumAll(t *testing.T) {
	got := SumAll([]int{1, 2, 3, 4, 5}, []int{1, 2, 3})
	want := []int{15, 6}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestSumAllTails(t *testing.T) {
	checkSums := func(t testing.TB, got, want []int) {
		t.Helper()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}
	}
	t.Run("sums two arrays tails", func(t *testing.T) {
		got := SumAllTails([]int{1, 2, 3, 4, 5}, []int{1, 2, 3})
		want := []int{14, 5}

		checkSums(t, got, want)
	})
	t.Run("safely sums an empty array against a non empty", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{2, 3, 4})
		want := []int{0, 7}

		checkSums(t, got, want)
	})
}
