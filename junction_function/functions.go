package junctionfunction

import "fmt"

/*

A parameter is passed by value if the function gets a copy; the caller can't see the changes to the copy

pass by value examples:

- numbers
- bool
- arrays
- structs

A parameter is passed by reference if the function can modify the actual parameter such that the caller sees changes

- slices
- maps
- channels
- things passed by pointer (&thing)
- strings (but they're immutable)


*/

// Below is pass by value because doArray didn't modify the array

func doArray(b [3]int) int {
	b[0] = 0
	return b[1]
}

// Below is pass by reference because doSlice did modify the array

func doSlice(b []int) int {
	b[0] = 0
	fmt.Printf("b@ %p\n", b)
	return b[1]
}

func doMap(m1 map[string]string) string {
	m1["matt"] = "cool"
	fmt.Printf("m1@ %p \n", m1)
	return m1["matt"]
}

func doMapChange(m2 map[int]int) {
	m2[3] = 0
	fmt.Printf("domapchange - m2@ %p %v \n", m2, m2)
	m2 = make(map[int]int)
	m2[4] = 4
	fmt.Printf("domapchange - m2@ %p %v \n", m2, m2)
}

func doMapChangePointer(m3 *map[int]int) {
	(*m3)[3] = 0
	*m3 = make(map[int]int)
	(*m3)[4] = 4
	fmt.Println("m3", *m3)
}

func DoChangeFuncs() {
	a := [3]int{1, 2, 3}
	v := doArray(a)
	fmt.Println(a, v)

	b := []int{1, 2, 3}
	r := doSlice(b)
	fmt.Printf("b@ %p\n", b)
	fmt.Println(b, r)

	m := map[string]string{"matt": "sick"}
	fmt.Printf("m1@ %p\n %v \n", m, m)
	p := doMap(m)
	fmt.Println(m, p)

	q := map[int]int{4: 1, 7: 2, 8: 3}
	fmt.Printf("m2@ %p %v \n", q, q)
	doMapChange(q)
	fmt.Printf("m2@ %p %v \n", q, q)

	j := map[int]int{4: 1, 7: 2, 8: 3}
	fmt.Println("m3", v)
	doMapChangePointer(&j)
	fmt.Println("m3", v)
}
