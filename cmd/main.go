package main

import (
	"hello/exercises"
	slicesdetail "hello/slices_detail"
)

func main() {
	// server := server.NewPlayerServer(server.NewInMemoryPlayerStore())
	// log.Fatal(http.ListenAndServe(":5000", server))

	// if len(os.Args) > 1 {
	// 	// If there's an argument, treat it as a filename and open it
	// 	file, err := os.Open(os.Args[1])
	// 	if err != nil {
	// 		fmt.Fprintf(os.Stderr, "Error opening file: %v\n", err)
	// 		os.Exit(1)
	// 	}
	// 	defer file.Close()
	// 	w.ScanWords(file)
	// 	c.CreateAndCat()
	// 	c.CreateAndCatTwo()
	// }
	// j.DoChangeFuncs()

	// closures.DoClosures()
	slicesdetail.DoSlices()
	exercises.DoCount()
}
