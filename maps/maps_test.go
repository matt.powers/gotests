package maps

import "testing"

func TestSearch(t *testing.T) {
	t.Run("search finds a word that is in our dictionary", func(t *testing.T) {
		dict := Dictionary{"test": "this is just a test"}

		got, err := dict.Search("test")
		assertNoError(t, err)

		want := "this is just a test"

		assertStrings(t, got, want)
	})

	t.Run("search errors when no word is found", func(t *testing.T) {
		dict := Dictionary{"nope": "sorry"}

		_, err := dict.Search("test")

		if err == nil {
			t.Fatalf("expected an error but didn't get one. Dictionary is: %v", dict)
		}

		assertError(t, err, ErrSearchNotFound)
	})

	t.Run("can successfully add a new word to the dictionary", func(t *testing.T) {
		dict := Dictionary{}

		_, err := dict.Search("hello")

		assertError(t, err, ErrSearchNotFound)

		err = dict.Add("test", "this is just a test")
		assertNoError(t, err)
		assertDefinition(t, dict, "test", "this is just a test")
	})

	t.Run("should return an error if trying to add a definition that already exists", func(t *testing.T) {
		dict := Dictionary{}

		err := dict.Add("test", "this is just a test")

		assertNoError(t, err)
		assertDefinition(t, dict, "test", "this is just a test")

		err = dict.Add("test", "this is NOT just a test")

		assertError(t, err, ErrAlreadyExists)
	})

	t.Run("should update a word that already exists", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"
		dict := Dictionary{word: definition}

		updatedDefintion := "this is not a test"

		err := dict.Update(word, updatedDefintion)

		assertNoError(t, err)

		assertDefinition(t, dict, word, updatedDefintion)
	})

	t.Run("should error if trying to update a word that doesn't exist", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"
		dict := Dictionary{word: definition}

		updatedDefintion := "this is not a test"
		fakeWord := "fail"

		err := dict.Update(fakeWord, updatedDefintion)

		assertError(t, err, ErrCantUpdate)

		assertDefinition(t, dict, word, definition)
	})
	t.Run("should delete a word if it exists", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"
		dict := Dictionary{word: definition}

		dict.Delete(word)

		_, err := dict.Search(word)

		assertError(t, err, ErrSearchNotFound)
	})
}

func assertStrings(t testing.TB, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q wanted %q", got, want)
	}
}

func assertNoError(t testing.TB, err error) {
	t.Helper()

	if err != nil {
		t.Errorf("got an error but didn't want one: %v", err)
	}
}

func assertError(t testing.TB, got, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("wanted an error but didn't get one")
	}
}

func assertDefinition(t testing.TB, dictionary Dictionary, word, definition string) {
	t.Helper()

	got, err := dictionary.Search(word)
	if err != nil {
		t.Fatal("should find added word: ", err)
	}
	assertStrings(t, got, definition)
}
