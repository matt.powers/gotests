package maps

import "errors"

type Dictionary map[string]string

type DictionaryErr string

var (
	ErrSearchNotFound = errors.New("sorry we couldn't find that")
	ErrAlreadyExists  = errors.New("that word already exists")
	ErrCantUpdate     = errors.New("cant update a word that doesn't exist yet")
)

func (e DictionaryErr) Error() string {
	return string(e)
}

func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]

	if !ok {
		return "", ErrSearchNotFound
	}

	return definition, nil
}

func (d Dictionary) Add(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrSearchNotFound:
		d[word] = definition
	case nil:
		return ErrAlreadyExists
	default:
		return err

	}
	return nil
}

func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrSearchNotFound:
		return ErrCantUpdate
	case nil:
		d[word] = definition
	default:
		return err

	}

	return nil
}

func (d Dictionary) Delete(word string) {
	delete(d, word)
}
