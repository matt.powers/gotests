package cat

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func CreateAndCat() {
	for _, fname := range os.Args[1:] {
		file, err := os.Open(fname)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}

		data, err := io.ReadAll(file)
		if err != nil {
			fmt.Fprint(os.Stderr, err)
			continue
		}

		fmt.Println("this file has ", len(data), "bytes")

		file.Close()
	}
}

// go run ./cmd/main.go a.txt
// go run ./cmd/main.go *.txt

func CreateAndCatTwo() {
	var tlc, twc, tcc int
	for _, fname := range os.Args[1:] {
		var lc, wc, cc int

		file, err := os.Open(fname)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}

		scan := bufio.NewScanner(file)

		for scan.Scan() {
			s := scan.Text()

			cc += len(s)
			wc += len(strings.Fields(s))
			lc++

		}

		tlc += lc
		twc += wc
		tcc += cc

		fmt.Printf("lines: %-5d words: %-5d characters: %-5d file name: %-5s \n", lc, wc, cc, fname)

		file.Close()
	}

	if len(os.Args[1:]) > 1 {
		fmt.Printf("lines: %-5d words: %-5d characters %-5d Total \n", tlc, twc, tcc)
	}
}
