package closures

import "fmt"

func DoClosures() {
	f := fib()

	for m := 0; m < 100; {
		m = f()
		fmt.Println(m)
	}

	s := make([]func(), 4)

	for i := 0; i < 4; i++ {
		i := i
		s[i] = func() {
			fmt.Printf("%d @ %p \n", i, &i)
		}
	}

	for i := 0; i < 4; i++ {
		s[i]()
	}
}

func fib() func() int {
	a, b := 0, 1

	return func() int {
		a, b = b, a+b

		return b
	}
}
