package hello

const (
	englishPrefix = "Hello, "
	englishWorld  = "world"
	spanishPrefix = "Hola, "
	spanishWorld  = "mundo"
	spanish       = "Spanish"
	french        = "French"
	frenchPrefix  = "Bonjour, "
	frenchWorld   = "monde"
)

func Hello(name string, language string) string {
	return greetingPrefix(language) + emptyNameCheck(name, language)
}

func emptyNameCheck(name string, language string) (checkedName string) {
	isEmpty := name == ""
	checkedName = name
	if isEmpty {
		switch language {
		case spanish:
			checkedName = spanishWorld
		case french:
			checkedName = frenchWorld
		default:
			checkedName = englishWorld
		}
	}
	return
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case spanish:
		prefix = spanishPrefix

	case french:
		prefix = frenchPrefix

	default:
		prefix = englishPrefix

	}
	return
}
