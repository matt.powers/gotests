package hello

import "testing"

func TestHello(t *testing.T) {
	t.Run("saying hello to matt returns Hello, Matt", func(t *testing.T) {
		got := Hello("Matt", "")
		want := "Hello, Matt"

		assertCorrectMessage(t, got, want)
	})

	t.Run("passing an empty string to Hello returns Hello, world", func(t *testing.T) {
		got := Hello("", "")
		want := "Hello, world"

		assertCorrectMessage(t, got, want)
	})

	t.Run("passing spanish with an empty name string returns Hola, mundo", func(t *testing.T) {
		got := Hello("", "Spanish")
		want := "Hola, mundo"
		assertCorrectMessage(t, got, want)
	})

	t.Run("passing french with an empty name string returns Bonjour, monde", func(t *testing.T) {
		got := Hello("", "French")
		want := "Bonjour, monde"
		assertCorrectMessage(t, got, want)
	})
}

func assertCorrectMessage(t testing.TB, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
