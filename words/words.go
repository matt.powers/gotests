package words

import (
	"bufio"
	"fmt"
	"io"
	"sort"
)

func ScanWords(r io.Reader) {
	scan := bufio.NewScanner(r)
	words := make(map[string]int)

	scan.Split(bufio.ScanWords)

	for scan.Scan() {
		word := scan.Text()
		words[word]++
	}

	type kv struct {
		key string
		val int
	}

	var ss []kv

	for k, v := range words {
		ss = append(ss, kv{k, v})
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].val > ss[j].val
	})

	for _, s := range ss[:3] {
		fmt.Println(s.key, "appears", s.val, "times")
	}

	fmt.Println(len(words), " - unique words")
}
