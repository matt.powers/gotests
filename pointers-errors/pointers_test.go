package pointerserrors

import "testing"

func assertCorrect(t testing.TB, got, want Bitcoin) {
	t.Helper()

	if got != want {
		t.Errorf("got %s want %s", got, want)
	}
}

func assertError(t testing.TB, got error, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("wanted an error but didn't get one")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func assertNoError(t testing.TB, got error) {
	t.Helper()

	if got != nil {
		t.Fatalf("got an error but didn't want one: %v", got)
	}
}

func TestWallet(t *testing.T) {
	t.Run("Can deposit 10 and get the balance of 10", func(t *testing.T) {
		wallet := Wallet{}

		wallet.Deposit(Bitcoin(10))

		got := wallet.Balance()
		want := Bitcoin(10)

		assertCorrect(t, got, want)
	})

	t.Run("Can deposit 10, withdraw 10, and get a balance of 0", func(t *testing.T) {
		wallet := Wallet{}

		wallet.Deposit(Bitcoin(10))

		err := wallet.Withdraw(Bitcoin(10))
		assertNoError(t, err)

		got := wallet.Balance()
		want := Bitcoin(0)

		assertCorrect(t, got, want)
	})

	t.Run("Get an error when we try to withdraw more than we have", func(t *testing.T) {
		startingBalance := Bitcoin(10)
		wallet := Wallet{startingBalance}

		got := wallet.Withdraw(Bitcoin(20))

		assertError(t, got, ErrInsufficientFunds)
	})
}
